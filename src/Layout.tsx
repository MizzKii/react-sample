import React from 'react'
import { Container } from 'react-bootstrap'
import './Layout.scss'
import { Layout } from './components'

interface LayoutProps {
  children: React.ReactNode,
}

const LayoutComponent: React.FC<LayoutProps> = ({ children }) => {
  return (
    <React.Fragment>
      <Layout.Navbar />
      <Container>{children}</Container>
    </React.Fragment>
  )
}

export default LayoutComponent
