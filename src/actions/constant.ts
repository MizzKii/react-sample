const generateAction = (key: string) => ({
  INSERT: `INSERT_${key}`,
  UPDATE: `UPDATE_${key}`,
  DELETE: `DELETE_${key}`,
})

const ACTIONS = {
  MESSAGE: generateAction('MESSAGE')
}

export default ACTIONS
