import ACTIONS from './constant'
import { Action } from './interface'

const update = (message: string): Action => ({
  type: ACTIONS.MESSAGE.UPDATE,
  payload: message,
})

export { update }
