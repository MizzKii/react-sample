import { combineProviders } from './Provider'
import { MessageContext, MessageProvider } from './message'

export const StoreProvider = combineProviders([MessageProvider])
export {
  MessageContext,
}
