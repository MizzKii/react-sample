import { Action, ACTIONS } from '../actions'
import { createContextProvider } from './Provider'

const initialState = 'initialStateMessage'

const reducer = (state: string, action: Action) => {
  switch (action.type) {
    case ACTIONS.MESSAGE.UPDATE:
      return action.payload
    default: return state
  }
}

const [MessageContext, MessageProvider] = createContextProvider(reducer, initialState)

export { MessageContext, MessageProvider }
