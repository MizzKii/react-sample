import React, { Fragment } from 'react'
import { Action } from '../actions'

interface StoreProps {
  children?: React.ReactNode
}

const combineProviders = (providers: Array<React.ComponentType<any>>) => ({ children }: StoreProps) => (
  <Fragment>
    {providers.reduce((current: React.ReactNode, Next: React.ComponentType) => <Next>{current}</Next>, children)}
  </Fragment>
)

interface ContextProviderProps {
  children: React.ReactNode,
}

interface Reducer {
  (state: string, action: Action): any
}

interface CreateProvider {
  (reducer: Reducer, initialState: any): [React.Context<[any, React.Dispatch<Action>]>, React.ComponentType<ContextProviderProps>]
}

const createContextProvider: CreateProvider = (reducer, initialState) => {
  const Context = React.createContext([ null, () => null ] as [any, React.Dispatch<Action>])
  const Provider = ({ children }: ContextProviderProps) => {
    const [state, dispatch] = React.useReducer(reducer, initialState)
    return (
      <Context.Provider value={[ state, dispatch ]}>
        {children}
      </Context.Provider>
    )
  }
  return [Context, Provider]
}

export { combineProviders, createContextProvider }
