import React from 'react'
import './Home.css'
import logo from '../assets/logo.svg'
import { MessageContext } from '../contexts'

const HomePage = () => {
  const [state] = React.useContext(MessageContext)
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <p>Message: {state}</p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  )
}

export default HomePage
