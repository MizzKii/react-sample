import React from 'react'
import { Form } from 'react-bootstrap'
import './Home.css'
import logo from '../assets/logo.svg'
import { messageAction } from '../actions'
import { MessageContext } from '../contexts'

const HomePage = () => {
  const [state, dispatch] = React.useContext(MessageContext)
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload. (Features page)
        </p>
        <p>Message: {state}</p>
        <Form.Control
          type="text"
          placeholder="Enter message"
          defaultValue={state as string}
          onChange={e => dispatch(messageAction.update(e.target.value))}
        />
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  )
}

export default HomePage
