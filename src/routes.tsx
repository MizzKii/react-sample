import React from 'react'

interface Route {
  path: string,
  props?: object,
  exact?: boolean,
  component: string,
}

const routes: Array<Route> = [
  { exact: true, path: '/', component: 'Home', props: {} },
  { path: '/features', component: 'Features' },
  { path: '/pricing', component: 'Pricing' },
]

const createRouteComponent = (page: string, props: object = {}) => {
  const Component = React.lazy(() => import(`./pages/${page}`))
  return () => <Component {...props} />
}

export default routes
export { createRouteComponent }
