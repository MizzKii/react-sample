import React from 'react'
import { Link } from 'react-router-dom'
import { Navbar, Nav, Container } from 'react-bootstrap'

interface NavbarProps {
  children?: React.ReactNode,
}

const NavbarComponent: React.FC<NavbarProps> = ({ children }) => {
  return (
    <Navbar fixed="top" bg="light" variant="light">
      <Container>
        <Navbar.Brand>
          <img alt="Logo" src="/logo192.png" width="30" height="30" />
          {' '}
          React Bootstrap
        </Navbar.Brand>
        {children}
        <Nav className="ml-auto">
          <Link className="nav-link" role="button" to="/">Home</Link>
          <Link className="nav-link" role="button" to="/features">Features</Link>
          <Link className="nav-link" role="button" to="/pricing">Pricing</Link>
        </Nav>
      </Container>
    </Navbar>
  )
}

export default NavbarComponent
