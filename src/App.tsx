import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Layout from './Layout'
import routes, { createRouteComponent } from './routes'

const LoadingPage = () => <label>Loading....</label>
const ErrorPage = () => <label>Not found....</label>

const App = () => {
  return (
    <React.Suspense fallback={<LoadingPage />}>
      <BrowserRouter>
        <Layout>
            <Switch>
              {routes.map((route, index) => (
                <Route
                  key={index}
                  exact={route.exact || false}
                  path={route.path}
                  component={createRouteComponent(route.component)}
                />
              ))}
              <Route component={ErrorPage} />
            </Switch>
        </Layout>
      </BrowserRouter>
    </React.Suspense>
  )
}

export default App
